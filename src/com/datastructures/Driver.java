package com.datastructures;

import com.datastructures.players.Player;
import java.util.Scanner;

public class Driver {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Player player = new Player();
        boolean exit = false;

        while (!exit){//If the user enters an invalid response this loop will notify and re-prompt
            System.out.println("****************************************\n" +
                    "\tWelcome to 'WHEEL OF FORTUNE'\n " +
                    " Enter 'P' to begin or 'Q' to quit\n" +
                    "****************************************\n" +
                    "\t\t\t\t  => ");
            char action = scan.next().charAt(0);//Reads user input
            action = Character.toUpperCase(action);//Converts the character input to uppercase in the event that it is in lower case.

            if (action == 'P') {//if the user inputs 'P' then the game will begin
                exit = true;
                player.createPlayer();//calls the create player method in the Player class
            } else if (action == 'Q') {//if the user inputs 'Q' then they will quit the game
                exit = true;
                System.exit(0);//terminates the JVM if the person enters Q
            } else {
                System.err.println("Error! Invalid input");//if the user inputs neither letters then they will receive this error message.
            }
        }
    }
}
