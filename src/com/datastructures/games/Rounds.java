package com.datastructures.games;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Rounds {
    private int Id;
    private String Category;
    private String Puzzle;

    //default constructor
    public Rounds() {
        Id = 0;
        Category = "";
        Puzzle = "";
    }

    //primary constructor
    public Rounds(int id, String category, String puzzle) {
        Id = id;
        Category = category;
        Puzzle = puzzle;
    }

    //copy constructor
    public Rounds(Rounds r) {
        Id = r.Id;
        Category = r.Category;
        Puzzle = r.Puzzle;
    }

    //accessors
    public int getId() {
        return Id;
    }

    public String getCategory() {
        return Category;
    }

    public String getPuzzle() {
        return Puzzle;
    }


    //mutators
    public void setId(int id) {
        Id = id;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public void setPuzzle(String puzzle) {
        Puzzle = puzzle;
    }

    @Override
    public String toString() {
        return "Rounds{" +
                "Id=" + Id +
                ", Category='" + Category + '\'' +
                ", Puzzle='" + Puzzle + '\'' +
                '}';
    }

    //Create the puzzles
    public void createPuzzle(){
        Rounds game1, game2, game3, game4, game5, game6, game7, game8, game9, game10, game11, game12;
        //The place category of the puzzle
        game1 = new Rounds(1, "Place", "THE MADRID UNDERGROUND");
        game2 = new Rounds(2, "Place", "HOUSES OF PARLIAMENT");
        game3 = new Rounds(3, "Place", "UNIVERSITY OF TECHNOLOGY");

        //The Person puzzle category
        game4 = new Rounds(4, "Person", "THE UKRAINIAN PRESIDENT");
        game5 = new Rounds(5, "Person", "SELF MADE WOMAN");
        game6 = new Rounds(6, "Person", "YOUR WIFES DAUGHTER");

        //Things category for the puzzle
        game7 = new Rounds(7, "Thing", "A BEAUTIFUL COCKTAIL DRESS");
        game8 = new Rounds(8, "Thing", "UNIDENTIFIED FLYING OBJECTS");
        game9 = new Rounds(9, "Thing", "TRACTOR TRAILER RIG");

        //The phase category for the puzzles
        game10 = new Rounds(10, "Phrase", "ASSORTMENT OF EMOTICONS");
        game11 = new Rounds(11, "Phrase", "WORST CASE SCENARIO");
        game12 = new Rounds(12, "Phrase", "UNSURPASSED NATURAL WONDERS");


        try{
            FileWriter writer = new FileWriter("GamePuzzles.txt", true);

            //Stores each object in a string variable to write to the fill
            String record1 = game1.getId() + "\t" + game1.getCategory() + "\t" + game1.getPuzzle() + "\n";
            String record2 = game2.getId() + "\t" + game2.getCategory() + "\t" + game2.getPuzzle() + "\n";
            String record3 = game3.getId() + "\t" + game3.getCategory() + "\t" + game3.getPuzzle() + "\n";
            String record4 = game4.getId() + "\t" + game4.getCategory() + "\t" + game4.getPuzzle() + "\n";
            String record5 = game5.getId() + "\t" + game5.getCategory() + "\t" + game5.getPuzzle() + "\n";
            String record6 = game6.getId() + "\t" + game6.getCategory() + "\t" + game6.getPuzzle() + "\n";
            String record7 = game7.getId() + "\t" + game7.getCategory() + "\t" + game7.getPuzzle() + "\n";
            String record8 = game8.getId() + "\t" + game8.getCategory() + "\t" + game8.getPuzzle() + "\n";
            String record9 = game9.getId() + "\t" + game9.getCategory() + "\t" + game9.getPuzzle() + "\n";
            String record10 = game10.getId() + "\t" + game10.getCategory() + "\t" + game10.getPuzzle() + "\n";
            String record11 = game11.getId() + "\t" + game11.getCategory() + "\t" + game11.getPuzzle() + "\n";
            String record12 = game12.getId() + "\t" + game12.getCategory() + "\t" + game12.getPuzzle() + "\n";

            //writes each string to the file
            writer.write(record1);
            writer.write(record2);
            writer.write(record3);
            writer.write(record4);
            writer.write(record5);
            writer.write(record6);
            writer.write(record7);
            writer.write(record8);
            writer.write(record9);
            writer.write(record10);
            writer.write(record11);
            writer.write(record12);

            System.out.println("Puzzle file created successfully!");
            writer.close();
        }catch(IOException e){
            System.err.println(e.getMessage());;
        }
    }






    public void readPuzzle(){
        try{
            FileReader reader = new FileReader("GamePuzzles.txt");
            BufferedReader buffer = new BufferedReader(reader);
            String record;
            String fields[] = null;
            while((record = buffer.readLine()) != null){
                fields = record.split("\t");
                int Id = Integer.parseInt(fields[0]);
                String Category = fields[1];
                String Puzzle = fields[2];
                System.out.println(Id + ", " + Category + ", " + Puzzle + ", \n");
            }
            buffer.close();
            reader.close();
        }catch(IOException e){
            System.err.println(e);
        }

    }
}
