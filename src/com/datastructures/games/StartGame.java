package com.datastructures.games;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Objects;
import java.util.Scanner;

public class StartGame {
    //To search for a puzzle at random for the players
    Scanner scan  = new Scanner(System.in);//Scanner cannot be used for strings again because the "next()" function clashes with the "nextLine()"
    boolean found;
//    int count1;
    String Puzzle = null;
    String puzzleFound = null;
    StringBuilder fillerWord;
    char blank ='_';//To represent the missing letters.
    char space = ' ';//To represent the spaces.

    public void findPuzzle() {

        int count = 0, check = 0, characterCount, prevWord=0, currWord=0, wordCount;
        //Generates a random number to search for a random puzzle to present the player with
        int rand = (int) (Math.random() * 12);


        try {//Searches the file to find the random puzzle.
            FileReader reader = new FileReader("GamePuzzles.txt");
            BufferedReader buffer = new BufferedReader(reader);
            String record;
            String fields[] = null;
            boolean found = false;//Boolean is initialized to false
            while ((record = buffer.readLine()) != null && !found) {//While the line is not empty and the search key is not found search the Id
                fields = record.split("\t");//splits the list with the \t delimiter
                if (Integer.parseInt(fields[0]) == rand) {//If the puzzle id matches the random number, do the following
                    found = true;//if the match is found then the boolean becomes true.
                }
            }
            if (found) {//if found is true then populate the variables with their assigned field
                int Id = Integer.parseInt(fields[0]);
                String Category = fields[1];
                Puzzle = fields[2];
                System.out.println(Id + ", " + Category + ", " + Puzzle + ", \n");
                puzzleFound = Puzzle;
            }
            buffer.close();
            reader.close();
        } catch (IOException e) {//if the file could not be opened or read then catch the error and present it to the user
            System.err.println(e);
        }

        //Finds the number of letters to make up the puzzle
        for (count = 0; count != Puzzle.length(); count++){
            char letter = Puzzle.charAt(count);//stores each letter found
            if (Objects.equals(letter, ' ') || count == (Puzzle.length()-1)){//If the current character is a space then do the following steps
                if(check == 0) {//If it gets to the first space, it prints the word before the space
                    currWord += count;
                    prevWord = count + 1;
                    System.out.println("First word has: " + currWord + " letters");
                }
                if(check == 1){//if it gets to the second space, it prints the word before the space
                    currWord = count - prevWord;
                    prevWord = count + 1;
                    System.out.println("Second word has: " + currWord + " letters");
                }
                if(check == 2){//if it gets to the third space, it prints the word before the space
                    if (count == (Puzzle.length()-1)){//if count is one less than the puzzle length, do the following
                        currWord = count - prevWord + 1;
                        prevWord = count + 1;
                        System.out.println("Third word has: " + currWord + " letters");
                    } else {//if it in not one less, then do the following.
                        currWord = count - prevWord;
                        prevWord = count + 1;
                        System.out.println("Third word has: " + currWord + " letters");
                    }
                }
                if(check == 3){//if it gets to the end of the string, it prints the final word
                    currWord = count - prevWord + 1;
                    System.out.println("Fourth word has: " + currWord + " letters");
                }
                if (count != (Puzzle.length() - 1)) {//Allows check to increment only if a space was found, to prevent an increment when its at the end of the string
                    check++;//increments by 1 if and only if a space is found.
                }
            }
        }
        characterCount = count - check;//Calculates how many letters make up the puzzle
        wordCount = check + 1;//Calculates how many words make up the puzzle
        System.out.println("The puzzle consists of " + wordCount + " words and a total of " + characterCount + " letters");

        //Creates a blank list for the player to see
        StringBuilder string = new StringBuilder(puzzleFound);//String builder
        for (int i = 0; i != puzzleFound.length(); i++){//To build the blank string for the players to fill
            if(puzzleFound.charAt(i) == space){//if the current character is a space then put a space
                string.setCharAt(i, space);//sets the space at this given index
            } else {//else if it is a character then replace with an underscore
                string.setCharAt(i, blank);//Sets the underscore at the index
            }
        }
        fillerWord = string;
        System.out.println(fillerWord);
        challengePlayer();
    }


    public void challengePlayer(){
        //prompt the user for letter or full word guess
        char direction = '$';//The players decision on how to answer
        char answerLetter;//The letter the player enters for their answer
        //String answerPhrase; //The Phrase answer the player gives
        boolean exit = false; //Boolean flag to exit the loop
        boolean stop = false;//Boolean flag to exit the loop


        while(!exit) {//while the exit boolean is false then do the following operations
            while (!stop) {

                System.out.println("What is your guess?\n 1) A Letter\n 2) The entire puzzle");
                direction = scan.next().charAt(0);//collects the players decision
                if (direction == '1') {
                    stop = true;
//                    System.exit(1);
                } else {
                    System.err.println("ERROR! INVALID ENTRY");
                    stop = false;
                }
                System.out.flush();
            }

            //switch case to check player answer
            switch (direction) {
                case '1'://Case of the player guessing letters
                    found = false;
                    System.out.println("Guess a letter => ");
//                    System.out.flush();
                    Scanner scanAnswerLetter = new Scanner(System.in);
                    answerLetter = scanAnswerLetter.next().charAt(0);//Accept the player's answer
                    answerLetter = Character.toUpperCase(answerLetter);//Handles the character case in the event the player uses lowercase.

                    for (int count1 = 0; count1 != Puzzle.length(); count1++) {//For zero to the length of the puzzle string
//                        System.out.println("here works test 1");
                        if (Puzzle.charAt(count1) == answerLetter) {//If the character in the current position is equal to the player's answer
//                            System.out.println("here works test 2");
                            found = true;//Found boolean becomes true
                            fillerWord.setCharAt(count1, answerLetter);//Replace the underscore with the letter that was given
                        } else if (Puzzle.charAt(count1) == space) {//If the character in the current position is a space
//                            System.out.println("here works test 3");
                            fillerWord.setCharAt(count1, space);//Then continue with that space;
                        } else if (Puzzle.charAt(count1) == blank) {//If the character in the current position is an underscore
//                            System.out.println("here works test 4");
                            fillerWord.setCharAt(count1, blank);//Continue with the underscore
                        }

                        String finalPuzzleAnswer = fillerWord.toString();//
                        if(finalPuzzleAnswer.equals(Puzzle)){
                            System.out.println("CONGRATULATIONS");
                            return;
//                            break;
                        }
                        if ((count1 == Puzzle.length() - 1) && (found == false)) {//If it's the end of the puzzle and no equivalent to the answer was found
//                            System.out.println("Incorrect! This Letter is not apart of the puzzle");
                            exit = true;//Turn the exit boolean to true.
                        }
                    }
                    System.out.println(fillerWord);
                    stop = false;//Re-initializes the boolean otherwise the option to select the guess type will be skipped
                    break;
                case '2':
//                    direction = '$';
                    Scanner pscanner = new Scanner(System.in);
                    found = false;
                    System.out.println("PHRASE => ");
                    String answerPhrase = pscanner.nextLine();
                    answerPhrase = answerPhrase.toUpperCase();

                    System.out.println("TESTER PRINTER");
                    System.out.flush();

//
                    if (answerPhrase.equals(Puzzle)) {
                        found = true;
                        System.out.println("Correct!!");
                    } else {
                        System.out.println("Incorrect!!");
                        exit = true;
                    }
                    break;
            }
        }
    }

    //for searching for the letter the player game
    public void findLetter(){
        //character compare the letters and print.

        //if case in the event the letter is present
            //multiply their money by the amount of letters that they got
            //print the underscore for the missing letter and put the given letters where they belong in the lineup
        //else if its not present the player loses their turn by moving the player two by calling a change player method
    }

    //for checking the words that the player gave
    public void confirmAnswer(){
        //Compare the answer given to the puzzle selected
        //if it is correct then the player gets their cash prize
        //multiply the number of characters by the money they have
    }
}
