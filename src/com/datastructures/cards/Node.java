package com.datastructures.cards;

public class Node {
    private Card Data;
    private Node NextNode;
    private Node PrevNode;

    //Default constructor
    public Node() {
        Data = new Card();
        NextNode = null;
        PrevNode = null;
    }

    //Primary constructor 1
    public Node(Card data, Node nextNode, Node prevNode) {
        Data = data;
        NextNode = nextNode;
        PrevNode = prevNode;
    }

    //Primary constructor 2
    public Node(Card data) {
        Data = data;
        NextNode = null;
        PrevNode = null;
    }

    //Copy constructor
    public Node(Node node) {
        Data = node.Data;
        NextNode = node.NextNode;
        PrevNode = node.PrevNode;
    }

    //Accessors
    Card getData() {
        return Data;
    }

    Node getNextNode() {
        return NextNode;
    }

    Node getPrevNode() {
        return PrevNode;
    }

    //Mutators
    void setData(Card data) {
        Data = data;
    }

    void setNextNode(Node nextNode) {
        NextNode = nextNode;
    }

    void setPrevNode(Node prevNode) {
        PrevNode = prevNode;
    }

    //Display
    void DisplayNode(){
        Data.DisplayCard();//Displays form the Card's class
    }

}
