package com.datastructures.cards;

import com.datastructures.games.StartGame;

import java.util.ArrayList;

public class Card {
    StartGame startGame = new StartGame();
    ArrayList<Card> cardList = new ArrayList();
    private int Id;
    private String Type;
    private int Value;

    //default constructor
    public Card() {
        Id = 0;
        Type = "";
        Value = 0;
    }

    //primary constructor
    public Card(int id, String type, int value) {
        Id = id;
        Type = type;
        Value = value;
    }

    //copy constructor
    public Card(Card c) {
        Id = c.Id;
        Type = c.Type;
        Value = c.Value;
    }

    //accessors
    public int getId() {
        return Id;
    }

    public String getType() {
        return Type;
    }

    public int getValue() {
        return Value;
    }

    //mutators
    public void setId(int id) {
        Id = id;
    }

    public void setType(String type) {
        Type = type;
    }

    public void setValue(int value) {
        Value = value;
    }


    //to string
    @Override
    public String toString() {
        return "Card{" +
                "\nId=" + Id +
                ",\nType='" + Type + '\'' +
                ",\nValue=" + Value +
                "\n}";
    }

    //display function
    public void DisplayCard(){
        System.out.println(this);
    }


    //Creates the cardList for the wheel
    public void createCards(){
        cardList.add(new Card(0, "Money", 2500));
        cardList.add(new Card(1, "Lose a Turn", 0));
        cardList.add(new Card(2, "Money", 600));
        cardList.add(new Card(3, "Money", 700));
        cardList.add(new Card(4, "Money", 600));
        cardList.add(new Card(5, "Money", 650));
        cardList.add(new Card(6, "Money", 500));
        cardList.add(new Card(7, "Money", 700));
        cardList.add(new Card(8, "Bankruptcy", 0));
        cardList.add(new Card(9, "Money", 600));
        cardList.add(new Card(10, "Money", 550));
        cardList.add(new Card(11, "Money", 500));
        cardList.add(new Card(12, "Money", 600));
        cardList.add(new Card(13, "Bankruptcy", 0));
        cardList.add(new Card(14, "Money", 650));
        cardList.add(new Card(15, "Money", 850));
        cardList.add(new Card(16, "Money", 2500));
        cardList.add(new Card(17, "Money", 2500));
        cardList.add(new Card(18, "Money", 2500));
        cardList.add(new Card(19, "Money", 2500));
        cardList.add(new Card(20, "Money", 2500));
        cardList.add(new Card(21, "Money", 2500));
        cardList.add(new Card(22, "Money", 2500));
        cardList.add(new Card(23, "Money", 2500));
        cardList.add(new Card(24, "Money", 2500));
        spinWheel();
    }

    //Shows all the card in the game
    public void showCards(){
        Card c = new Card();
        for (int i = 0; i<cardList.size(); i++){
            c = cardList.get(i);
            System.out.println(c);
        }
    }


    //THIS METHOD WILL SPIN THE CARDS/ SPIN THE WHEEL
    public void spinWheel(){
        Card c = new Card();//new instance of the card class/object
        int pointer = 0;//points to the random card after the wheel spins.
        int rand = (int) (Math.random() * (100-50+1)+50);
//        int tracker;

        for (int i = 0; i != rand; i++){
//            pointer++;
            if (pointer == 25){
                pointer = 0;
            }
        }

//        System.out.println(cardList.get(pointer));
        System.out.println(cardList.get(pointer).getValue());
        startGame.findPuzzle();//calls the start game function to start the round.

    }
}
