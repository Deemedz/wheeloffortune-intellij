package com.datastructures.cards;

public class DoublyLinkedList {
    //points to the first element in the list
    private Node Head;
    private Node Tail;

    //Default constructor
    public DoublyLinkedList() {
        Head = null;
        Tail = null;
    }

    //Primary constructor
    public DoublyLinkedList(Node head, Node tail) {
        Head = head;
        Tail = tail;
    }

    //Copy constructor
    public DoublyLinkedList(DoublyLinkedList list) {
        Head = list.Head;
        Tail = list.Tail;
    }

    //Accessors
    Node getHead() {
        return Head;
    }
    Node getTail() {
        return Tail;
    }

    //Mutators
    void setHead(Node head) {
        Head = head;
    }
    void setTail(Node tail) {
        Tail = tail;
    }

    //INSERT AT THE BACK
    public void InsertAtBack(Card dataToInsert){
//        System.out.println(dataToInsert.getName());
        Node temp = new Node(dataToInsert);//Declares a new node for the list
//        System.out.println(curr);
        if(temp != null){//if the new node was created
            if(Head == null){//if head was empty
                Head = Tail = temp;//Points head to the memory address of head
//                System.out.println(Head);
            }else{//if head was not empty
                temp.setPrevNode(Tail);
                Tail.setNextNode(temp);
                Tail = temp;
//                Node trav = Head;//declares a temporary node to traverse the list
//                while(trav.getNextNode() != null){//checks if trav is pointing to the last node in the list
//                    trav = trav.getNextNode();//moves trav to the next node in the list
//                }
//                trav.setNextNode(curr);//makes the new node the last node in the list
            }
        }else{//if the node was not created
            System.err.println("Error! Memory is full. Cannot create node");//message if the node could not be created
        }
//        String message = System.out.println("Update added");
//        return (dataToInsert.getName());getName
    }


    //DISPLAY LIST
    public void DisplayList(){
        if (Head != null){
            Node curr = Head;
            System.out.println("null");
            while(curr != null){
                System.out.println("<-[ * | " + curr.getData() + " * ]->");
                curr = curr.getNextNode();
            }
            System.out.println("null");
        }else{
            System.err.println("Error! List is empty; nothing to display");
        }
//        Node trav = Head;//creates a temporary node to traverse the list of Cards
//        while(trav != null) {//while trav is pointing to an existing node
//            trav.DisplayNode();//displays the Card data from the node
//            trav = trav.getNextNode();//moves to the next node in the list
//        }
        //System.out.println("null");
    }



    //COUNT NODES
    int CountNodes()
    {
        int count = 0;//declare variable to keep track of elements in list

        Node trav = Head;//create a temporary node to traverse the list - start at front
        while(trav != null)//while trav is pointing to a valid node
        {
            count++;
            // trav.Display();//display the data for the node
            trav = trav.getNextNode();//move to the next node in the list
        }
        // System.out.println("NULL");
        return count;
    }

    //Inserts the information at the back
}
