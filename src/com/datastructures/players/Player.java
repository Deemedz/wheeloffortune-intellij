package com.datastructures.players;

import com.datastructures.cards.Card;

//import java.io.IOException;
import java.util.Scanner;

public class Player {
    int playerSelector = 1;
    Card card = new Card();
    SinglyLinkedList list = new SinglyLinkedList();//global call in this class for the use of the list
    //Player attributes
    private int Number;
    private String Name;
    private int GrandTotal;

    //Default constructor
    public Player() {
        Number = 0;
        Name = "";
        GrandTotal = 0;
    }

    //Primary constructor
    public Player(int number, String name, int grandTotal) {
        Number = number;
        Name = name;
        GrandTotal = grandTotal;
    }

    //Copy constructor
    public Player(Player p) {
        Number = p.Number;
        Name = p.Name;
        GrandTotal = p.GrandTotal;
    }

    //Accessors
    public int getNumber() {
        return Number;
    }

    public String getName() {
        return Name;
    }

    public int getGrandTotal() {
        return GrandTotal;
    }

    //Mutators
    public void setNumber(int number) {
        Number = number;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setGrandTotal(int grandTotal) {
        GrandTotal = grandTotal;
    }

    //To String
    @Override
    public String toString() {
        return "Player{" +
                "Number=" + Number +
                ", Name='" + Name + '\'' +
                ", GrandTotal=" + GrandTotal +
                '}';
    }

    //Display
    public void DisplayPlayer(){
        System.out.println("{\n\tPlayer Number: " + Number + ", \n\tPlayer Name: " + Name + ", \n\tPlayer Total: " + GrandTotal + "\n}");
    }


    //The first screen the user will see after opening the program
    public void welcomeScreen(){
        //SystemClearScreen
//        Scanner scan = new Scanner(System.in);
//        System.out.println("Welcome to 'WHEEL OF FORTUNE'.\n Enter 'P' to begin or 'Q' to quit");
//        String action = scan.nextLine();
//        action = action.toUpperCase();
//
//        if(action.equals("P")){//if the user inputs 'P' then the game will begin
////            System.out.print("\u000C");
//            createPlayer();
//        }else if(action.equals('Q')){//if the user inputs 'Q' then they will quit the game
////            Quit();
//        }else{
//            System.err.println("Error! Invalid input");//if the user inputs neither letters then they will receive this error message.
//        }
    }


    //The section that will prompt the players for their names
    public void createPlayer() {//adds the name of each player as well as their generated information to a linkedlist.
        Scanner scan = new Scanner(System.in);
//        System.out.print("\033[H\033[2J");
//        System.out.flush();
//        ConsoleClearScreen clearScreen = new ConsoleClearScreen();
//        clearScreen.ClearScreen();


        for(int x=1; x<4; x++){
            System.out.println("Player " + x + "'s name --> ");
            Name = scan.nextLine();
            setNumber(x);//assigns the players' number
            setGrandTotal(0);//assigns the grand total for the players'

            list.InsertAtBack(new Player(Number, Name, GrandTotal));//adds the player detail to the list
        }
    }

    //This function displays all the player nodes in the list
    void showPlayers(){
        list.DisplayList();
    }




    //THIS METHOD SELECTS A NEW PLAYER EACH TIME
    public void selectPlayer(){
        while(playerSelector <= 3){//while the selector's value is less than or equal to 4, do the following
            if(playerSelector > 3){//if selector's value is greater than 3, do the following
                playerSelector = 1;//initialize player selector to 1.
            }
            list.SearchPlayer(playerSelector);//calls linked-list function/method.
//            System.out.println("check entry space");

            card.createCards();//calls the card function to create and spin the wheel.
            playerSelector++;//increments the selector by 1 each time this method is entered.
        }
    }
}
