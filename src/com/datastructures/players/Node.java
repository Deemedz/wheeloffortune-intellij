package com.datastructures.players;

public class Node {
    private Player Data;
    private Node NextNode;
    private Node PrevNode;

    //Default constructor
    public Node() {
        Data = new Player();
        NextNode = null;
        PrevNode = null;
    }

    //Primary constructor 1
    public Node(Player data, Node nextNode, Node prevNode) {
        Data = data;
        NextNode = nextNode;
        PrevNode = prevNode;
    }

    //Primary constructor 2
    public Node(Player data) {
        Data = data;
        NextNode = null;
        PrevNode = null;
    }

    //Copy constructor
    public Node(Node node) {
        Data = node.Data;
        NextNode = node.NextNode;
        PrevNode = node.PrevNode;
    }

    //Accessors
    Player getData() {
        return Data;
    }

    Node getNextNode() {
        return NextNode;
    }

    Node getPrevNode() {
        return PrevNode;
    }

    //Mutators
    void setData(Player data) {
        Data = data;
    }

    void setNextNode(Node nextNode) {
        NextNode = nextNode;
    }

    void setPrevNode(Node prevNode) {
        PrevNode = prevNode;
    }

    //Display
    void DisplayNode(){
        Data.DisplayPlayer();//Displays form the player's class
    }
}
